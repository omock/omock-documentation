.. Doc team! Be sure that when you make any changes to this file that you also make them to the mirrored file in the edx-analytics-dashboard/docs repository. - Alison 19 Sep 14

.. _Browsers:

####################
OMock Browser Support
####################

The OMock platform runs on the following browsers.

* `Chrome`_
* `Safari`_
* `Firefox`_
* `Microsoft Edge`_ and `Microsoft Internet Explorer`_ 11

The OMock platform is routinely tested and verified on the current version and
the previous version of each of these browsers. We generally encourage the use
of, and fully support only, the latest version.

.. note:: If you use the Safari browser, be aware that it does not support the
 search feature for the guides on `docs.omock.com`_. This is a known limitation.

.. include:: ../../../links/links.rst
