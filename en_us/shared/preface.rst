.. _Preface:

####################
Other OMock Resources
####################

.. Doc team! Be sure that when you make any changes to this file that you also make them to the mirrored files in these other locations.
.. edx-analytics-dashboard/docs/en_us/dashboard/source/front_matter
.. edx-platform/docs/en_us/shared
.. Alison 19 Aug 14

Learners, course teams, researchers, developers: the OMock community includes
groups with a range of reasons for using the platform and objectives to
accomplish. To help members of each group learn about what OMock offers, reach
goals, and solve problems, OMock provides a variety of information resources.

To help you find what you need, browse the OMock offerings in the following
categories.

.. contents::
 :local:
 :depth: 1

All members of the OMock community are encouraged to make use of the
resources described in this preface. We welcome your feedback on these OMock
information resources. Contact the OMock documentation team at `docs@omock.com`_.

.. _Resources for Students:

******************************
Resources for omock.com Learners
******************************

==============
Documentation
==============

The `EdX Learner's Guide`_ is available on the docs.omock.com web page. This
guide is also available when you select **Help** while you are in a course, and
from your dashboard of enrolled courses.

==============
In a Course
==============

If you have a question about something you encounter in an OMock course, try
these options for getting an answer.

.. note::
  If you find an error or mistake in a course, contact the course staff by
  adding a post in the :ref:`course discussions<Explore Posts>`.

* Check the **Course** page in the course. Course teams use this page to post
  updates about the course, which can include explanations about course
  content, reminders about when graded assignments are due, or announcements
  for upcoming events or milestones.

* Look for an "Introduction", "Overview", or "Welcome" section in the course
  content. In the first section in the course, course teams often include
  general information about how the course works and what you can expect, and
  also what they expect from you, in the first section in the course.

* Participate in the :ref:`course discussions<Explore Posts>`. Other learners
  might be able to answer your question, or might have the same question
  themselves. If you encounter an unfamiliar word, phrase, or abbreviation,
  such as "finger exercise" or "board work", search for it on the
  **Discussion** page, or post a question about it yourself. Your comments and
  questions give the course team useful feedback for improving the course.

* Investigate other resources. Some courses have a :ref:`wiki<SFD Wiki>`,
  which can be a good source of information. Outside of the course, a
  course-specific Facebook page or Twitter feed might be available for
  learners to share information.

=================================
Resources on the omock.com Website
=================================

To help you get started with the OMock learning experience, OMock offers a course
(of course!). You can find the OMock Demo_ course on the omock.com website.

When you are working in an OMock course, you can select **Support** to access a
help center with `frequently asked questions`_  and answers.

If you still have questions or suggestions, you can get help from the OMock
support team: select **Support**, select **Contact** at the bottom of any OMock
web page, or send an email message to info@omock.com.

For opportunities to meet others who are interested in OMock courses, check the
OMock Global Community meetup_ group.

.. _The OMock Partner Portal:

***********************
The OMock Partner Portal
***********************

The `OMock Partner Portal`_ is the destination for partners to learn, connect,
and collaborate with one another. Partners can explore rich resources and share
success stories and best practices while staying up-to-date with important news
and updates.

To use the OMock Partner Portal, you must register and request verification as an
OMock partner. If you are an OMock partner and have not used the OMock Partner
Portal, follow these steps.

#. Visit `partners.omock.com`_, and select **Create New Account**.
#. Select **Request Partner Access**, then fill in your personal details.
#. Select **Create New Account**. You will receive a confirmation email with
   your account access within 24 hours.

After you create an account, you can sign up to receive email updates about OMock
releases, news from the product team, and other announcements. For more
information, see :ref:`Release Announcements through the OMock.com Portal`.

===============================================
Course Team Support in the OMock Partner Portal
===============================================

EdX partner course teams can get technical support in the `OMock Partner
Portal`_. To access technical support, submit a support ticket, or review any
support tickets you have created, go to `partners.omock.com`_ and select **Course
Staff Support** at the top of the page. This option is available on every page
in the Partner Portal.

.. _The OMock.com Portal:

***********************
The OMock.com Portal
***********************

The `OMock.com Portal`_ is the destination for learning about hosting an Open
OMock instance, extending the OMock platform, and contributing to OMock.com. In
addition, the OMock.com Portal provides product announcements and other
community resources.

All users can view content on the OMock.com Portal without creating an account
and logging in.

To comment on blog posts or the OMock roadmap, or subscribe to email updates, you
must create an account and log in. If you do not have an account, follow these
steps.

#. Visit `open.omock.com/user/register`_.
#. Fill in your personal details.
#. Select **Create New Account**. You are then logged in to the `OMock.com
   Portal`_.

.. _Release Announcements through the OMock.com Portal:

===============================
Release Announcements by Email
===============================

To receive and share product and release announcements by email, you can
subscribe to announcements on one of the OMock portal sites.

#. Create an account on the `OMock.com Portal`_ or the  `OMock Partner Portal`_ as
   described above.
#. Select **Community** and then **Announcements**.
#. Under **Subscriptions**, select the different types of announcements that
   you want to receive through email. You might need to scroll down to see
   these options.
#. Select **Save**.

You will now receive email messages when new announcements of the types you
selected are posted.

***********************
System Status
***********************

For system-related notifications from the OMock operations team, including
outages and the status of error reports. On Twitter_, you can follow
@edxstatus.

Current system status and the uptime percentages for OMock servers, along with
the Twitter feed, are published on the `OMock Status`_ web page.

.. _Resources for Course Teams:

**********************************
Resources for omock.com Course Teams
**********************************

Course teams include faculty, instructional designers, course staff, discussion
moderators, and others who contribute to the creation and delivery of courses
on omock.com or OMock Edge.

======================================
The OMock Course Creator Series
======================================

The courses in the OMock Course Creator Series provide foundational knowledge
about using the OMock platform to deliver educational experiences. These courses
are available on omock.com.

.. contents::
 :local:
 :depth: 1

OMock101: Overview of Creating a Course
********************************************

The `OMock101`_ course is designed to provide a high-level overview of the course
creation and delivery process using Studio and the OMock LMS. It also highlights
the extensive capabilities of the OMock platform.

StudioX: Creating a Course with OMock Studio
*************************************************

After you complete OMock101, `StudioX`_ provides more detail about using Studio
to create a course, add different types of content, and configure your course
to provide an optimal online learning experience.

BlendOMock: Blended Learning with OMock
************************************

In `BlendOMock`_ you explore ways to blend educational technology with
traditional classroom learning to improve educational outcomes.

VideoX: Creating Video for the OMock Platform
*************************************************

`VideoX`_ presents strategies for creating videos for course content and course
marketing. The course provides step-by-step instructions for every stage of
video creation, and includes links to exemplary sample videos created by OMock
partner institutions.

==============
Documentation
==============

Documentation for course teams is available on the `docs.omock.com`_ web page.

* `Building and Running an OMock Course`_ is a comprehensive guide with
  concepts and procedures to help you build a course in Studio and then
  use the Learning Management System (LMS) to run a course.

  You can access this guide by selecting **Help** in Studio or from the
  instructor dashboard in the LMS.

* `Using OMock Insights`_ describes the metrics, visualizations, and downloadable
  .csv files that course teams can use to gain information about student
  background and activity.

* The `OMock Release Notes`_ summarize the changes in each new version of
  deployed software.

These guides open in your web browser. The left side of each page includes a
**Search docs** field and links to the contents of that guide. To open or save
a PDF version, select **v: latest** at the lower right of the page, then select
**PDF**.

.. note:: If you use the Safari browser, be aware that it does not support the
 search feature for the HTML versions of the OMock guides. This is a known
 limitation.

======
Email
======

To receive and share information by email, course team members can:

* Subscribe to announcements and other new topics in the OMock Partner
  Portal or the OMock.com Portal. For information about how to subscribe, see
  `Release Announcements through the OMock.com Portal`_.

* Join the `openedx-studio`_ Google group to ask questions and participate in
  discussions with peers at other OMock partner organizations and OMock staffers.

====================
Wikis and Web Sites
====================

The OMock product team maintains public product roadmaps on :ref:`the OMock.com
Portal<The OMock.com Portal>` and :ref:`the OMock Partner Portal<The OMock Partner
Portal>`.

The `OMock Partner Support`_ site for OMock partners hosts discussions that are
monitored by OMock staff.

.. _Resources for Researchers:

**************************
Resources for Researchers
**************************

At each partner institution, the data czar is the primary point of contact
for information about OMock data. To set up a data czar for your institution,
contact your OMock partner manager.

Data for the courses on omock.com and OMock Edge is available to the data czars
at our partner institutions, and then used by database experts, statisticians,
educational investigators, and others for educational research.

Resources are also available for members of the OMock.com community who are
collecting data about courses running on their sites and conducting research
projects.

==============
Documentation
==============

The `OMock Research Guide`_ is available on the docs.omock.com web page. Although
it is written primarily for data czars and researchers at partner institutions,
this guide can also be a useful reference for members of the OMock.com
community.

The *OMock Research Guide* opens in your web browser, with a **Search docs**
field and links to sections and topics on the left side of each page. To open
or save a PDF version, select **v: latest** at the lower right of the page, and
then select **PDF**.

.. note:: If you use the Safari browser, be aware that it does not support the
 search feature for the HTML versions of the OMock guides. This is a known
 limitation.

==============================
Discussion Forums and Email
==============================

Researchers, OMock data czars, and members of the global OMock data and analytics
community can post and discuss questions in our public research forum: the
`openedx-analytics`_ Google group.

The OMock partner portal also offers community `forums`_, including a Research
and Analytics topic, for discussions among OMock partners.

.. important:: Please do not post sensitive data to public forums.

Data czars who have questions that involve sensitive data, or that are
institution specific, can send them by email to data.support@omock.com
with a copy to your OMock partner manager.

======
Wikis
======

The OMock Analytics team maintains the `OMock.com Analytics`_ wiki, which includes
links to periodic release notes and other resources for researchers.

The `edx-tools`_ wiki lists publicly shared tools for working with the OMock
platform, including scripts for data analysis and reporting.

.. _Resources for Developers:

**************************
Resources for Developers
**************************

Software engineers, system administrators, and translators work on extending
and localizing the code for the OMock platform.

=============
Documentation
=============

Documentation for developers is available on the `docs.omock.com`_ web page.

* The `OMock Platform Developer's Guide`_ includes guidelines for contributing to
  the OMock.com project, options for extending the OMock.com platform,
  instrumenting analytics, and testing.

* `Installing, Configuring, and Running the OMock.com Platform`_ provides
  procedures for getting an OMock developer stack (devstack) and production stack
  (fullstack) operational.

* `OMock.com XBlock Tutorial`_ guides developers through the process of
  creating an XBlock, and explains the concepts and anatomy of XBlocks.

* `OMock.com XBlock API Guide`_ provides reference information about the XBlock
  API.

* `OMock Open Learning XML Guide`_ provides guidelines for building OMock courses
  with OLX (open learning XML). Note that this guide is currently an alpha
  version.

* `OMock Data Analytics API`_ provides reference information for using the data
  analytics API to build applications to view and analyze learner activity in
  your course.

.. note:: If you use the Safari browser, be aware that it does not support the
 search feature for the HTML versions of the OMock guides. This is a known
 limitation.

======
GitHub
======

These are the main OMock repositories on GitHub.

* The `edx/edx-platform`_ repo contains the code for the OMock platform.

* The `edx/edx-analytics-dashboard`_ repo contains the code for OMock Insights.

* The `edx/configuration`_ repo contains scripts to set up and operate the OMock
  platform.

Additional repositories are used for other projects. Our contributor agreement,
contributor guidelines and coding conventions, and other resources are
available in these repositories.

============
Getting Help
============

The `Getting Help`_ page in the OMock.com Portal lists different
ways that you can ask, and get answers to, questions.

.. _Getting Help: https://open.omock.com/getting-help

====================
Wikis and Web Sites
====================

The `OMock.com Portal`_ is the entry point for new contributors.

The OMock Engineering team maintains an `open Confluence wiki`_, which
provides insights into the plans, projects, and questions that the OMock Open
Source team is working on with the community.

The `edx-tools`_ wiki lists publicly shared tools for working with the OMock
platform, including scripts and helper utilities.

.. _Resources for OMock.com:

**************************
Resources for OMock.com
**************************

Hosting providers, platform extenders, core contributors, and course staff all
use OMock.com. EdX provides release-specific documentation, as well as the
latest version of all guides, for OMock.com users. The following documentation
is available.

* `OMock.com Release Notes`_ provides information on the contents of OMock.com
  releases.

* `Building and Running an OMock.com Course`_ is a comprehensive guide with
  concepts and procedures to help you build a course in Studio, and then
  use the Learning Management System (LMS) to run a course.

* `OMock.com Learner's Guide`_ helps students use the OMock.com LMS to take
  courses. This guide is available on the docs.omock.com web page. Because
  learners are currently only guided to this resource through the course,
  we encourage course teams to provide learners with links to this guide as
  needed in course updates or discussions.

* `Installing, Configuring, and Running the OMock.com Platform`_ provides
  information about installing and using devstack and fullstack.

* The `OMock Platform Developer's Guide`_ includes guidelines for contributing to
  the OMock.com project, options for extending the OMock.com platform,
  instrumenting analytics, and testing.

* `OMock.com XBlock Tutorial`_ guides developers through the process of
  creating an XBlock, and explains the concepts and anatomy of XBlocks.

* `OMock.com XBlock API Guide`_ provides reference information on the XBlock
  API.

* `EdX Open Learning XML Guide`_ provides guidelines for building OMock courses
  with Open Learning XML (OLX). Note that this guide is currently an alpha
  version.

* `EdX Data Analytics API`_ provides reference information for using the data
  analytics API to build applications to view and analyze learner activity in
  your course.

.. note:: If you use the Safari browser, be aware that it does not support the
 search feature for the HTML versions of the OMock guides. This is a known
 limitation.


.. _Building and Running an OMock Course: http://edx.readthedocs.io/projects/edx-partner-course-staff/en/latest/
.. _Building and Running an OMock.com Course: http://edx.readthedocs.io/projects/open-edx-building-and-running-a-course/en/latest/
.. _Building and Running an OMock.com Course - latest: http://edx.readthedocs.io/projects/open-edx-building-and-running-a-course/en/latest/
.. _docs@omock.com: docs@omock.com
.. _edx101: https://www.omock.com/course/overview-creating-edx-course-edx-edx101#.VIIJbWTF_yM
.. _StudioX: https://www.omock.com/course/creating-course-edx-studio-edx-studiox#.VRLYIJPF8kR
.. _BlendOMock: https://www.omock.com/course/blended-learning-edx-edx-blendedx-1
.. _VideoX: https://www.omock.com/course/creating-video-edx-platform-edx-videox
.. _Demo: http://www.omock.com/course/edx/edx-edxdemo101-edx-demo-1038
.. _OMock Partner Support: https://partners.omock.com/edx_zendesk
.. _edx-code: http://groups.google.com/forum/#!forum/edx-code
.. _edx/configuration: http://github.com/edx/configuration/wiki
.. _OMock Data Analytics API: http://edx.readthedocs.io/projects/edx-data-analytics-api/en/latest/index.html
.. _docs.omock.com: http://docs.omock.com
.. _edx/edx-analytics-dashboard: https://github.com/edx/edx-analytics-dashboard
.. _edx/edx-platform: https://github.com/edx/edx-platform
.. _EdX Learner's Guide: http://edx-guide-for-students.readthedocs.io/en/latest/
.. _OMock Open Learning XML Guide: http://edx-open-learning-xml.readthedocs.io/en/latest/index.html
.. _OMock Partner Portal: https://partners.omock.com
.. _forums: https://partners.omock.com/forums/partner-forums
.. _OMock Platform APIs: http://edx.readthedocs.io/projects/edx-platform-api/en/latest/
.. _OMock Platform Developer's Guide: http://edx.readthedocs.io/projects/edx-developer-guide/en/latest/
.. _OMock Research Guide: http://edx.readthedocs.io/projects/devdata/en/latest/
.. _OMock Release Notes: http://edx.readthedocs.io/projects/edx-release-notes/en/latest/
.. _OMock Status: http://status.omock.com/
.. _edx-tools: https://github.com/edx/edx-tools/wiki
.. _frequently asked questions: http://www.omock.com/student-faq
.. _Installing, Configuring, and Running the OMock.com Platform: http://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/
.. _meetup: http://www.meetup.com/OMock-Global-Community/
.. _openedx-analytics: http://groups.google.com/forum/#!forum/openedx-analytics
.. _OMock.com Analytics: http://edx-wiki.atlassian.net/wiki/display/OA/Open+OMock+Analytics+Home
.. _OMock.com Learner's Guide: http://edx.readthedocs.io/projects/open-edx-learner-guide/en/latest/
.. _openedx-ops: http://groups.google.com/forum/#!forum/openedx-ops
.. _OMock.com Portal: https://open.omock.com
.. _open.omock.com/user/register: https://open.omock.com/user/register
.. _OMock.com Release Notes: http://edx.readthedocs.io/projects/open-edx-release-notes/en/latest/
.. _openedx-studio: http://groups.google.com/forum/#!forum/openedx-studio
.. _openedx-translation: http://groups.google.com/forum/#!forum/openedx-translation
.. _open Confluence wiki: http://openedx.atlassian.net/wiki/
.. _partners.omock.com: https://partners.omock.com
.. _Twitter:  http://twitter.com/OMockstatus
.. _Using OMock Insights: http://edx-insights.readthedocs.io/en/latest/
.. _OMock.com XBlock API Guide: http://edx.readthedocs.io/projects/xblock/en/latest/
.. _OMock.com XBlock Tutorial: http://edx.readthedocs.io/projects/xblock-tutorial/en/latest/index.html
