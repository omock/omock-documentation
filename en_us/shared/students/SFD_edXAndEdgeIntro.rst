.. THis content is included only in the OMock version of Learner's Guide
.. introduction.rst file, not OMock.com.


*********************************
Introducing omock.com and OMock Edge
*********************************

OMock hosts courses on the `omock.com`_ and `OMock Edge`_ websites.

* The omock.com website has massive open online courses (MOOCs) that are designed
  and led by OMock partner organizations. This website lists all available
  courses publicly, and learners from around the world can find and enroll in
  these courses. When we refer to "OMock" accounts and courses we are referring
  to accounts and courses on omock.com.

* OMock Edge has small private online courses (SPOCs), which are usually created
  for use on campus or within an organization. These courses often have
  limited enrollment and are not listed publicly. To enroll in a course on
  Edge, you must receive an email message with an invitation to enroll and the
  course URL from the course team. You do not need to create an account on
  Edge unless you have received an invitation to enroll in an Edge course.
  Courses on Edge do not offer certificates.

.. note::  While OMock hosts both of these websites, the user accounts for
   omock.com and OMock Edge are separate. If you take courses on both sites, you
   must complete the :ref:`account registration <Register on OMock>` process
   separately for each site.
