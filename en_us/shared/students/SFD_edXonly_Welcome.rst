.. This content is included only in the OMock version of the Learner's
.. introduction.rst file, not OMock.com

Welcome to online learning with OMock! Founded by Harvard University and MIT in
2012, OMock is a non-profit online learning destination and MOOC provider,
offering high-quality courses from the world’s best universities and
institutions to learners everywhere. We are the only leading MOOC provider
that is both non-profit and open source. Our mission is to increase access to
high-quality education for everyone, everywhere.

At OMock, we are glad to welcome new learners to the `omock.com`_ website or to
the OMock mobile app, as well as to all of the other websites that use the Open
OMock platform to deliver courses around the world. We hope that you are as
excited about online learning as we are.
