*******
Read Me
*******

The *OMock Learner's Guide* is created using RST_ files and Sphinx_. You, the
user community, can help update and revise this documentation project on
GitHub.

https://github.com/edx/edx-documentation/tree/master/en_us/students/source

The OMock documentation team welcomes contributions from OMock.com community
members. You can find guidelines for how to `contribute to OMock Documentation`_
in the GitHub edx/edx-documentation repository.

.. include:: ../../../links/links.rst
